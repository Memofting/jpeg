import os
import sys
import argparse
import collections
import struct


class ImageData:
    """
    Позволяет произвести работу по декодировке картинки.
    """
    def __init__(self, image_bytes: bytes=b''):
        self.bytes = image_bytes
    
    def __str__(self):
        return "Size of raw image: {}".format(str(len(self.bytes)))


class MarkerInfo:
    """
    Позволяет упростить работу с форматом маркеров.
    """
    SOI = b'\xff\xd8'
    SOS = b'\xff\xda'  
    EOI = b'\xff\xd9'
    SOF0 = b'\xff\xc0'
    marker_info = {
        b'\xff\xd8': ('SOI', 'Start of image.'),
        b'\xff\xe0': ('APP0', 'Application-specific. JFIF.'),
        b'\xff\xe1': ('APP1', 'Application-specific. EXIF.'),
        b'\xff\xdb': ('DQT', 'Define Quantization Table(s).'),
        b'\xff\xc0': ('SOf0', 'Start Of Frame (baseline DCT).'),
        b'\xff\xc2': ('SOf2', 'Start Of Frame (progressive DCT).'),
        b'\xff\xc4': ('DHT', 'Define Huffman Table(s).'),  
        b'\xff\xda': ('SOS', 'Start Of Scan.'),
        b'\xff\xfe': ('COM', 'Comment.'), 
        b'\xff\xd9': ('EOI', 'End Of Image.'),
    }
    def __init__(self, name: str='', size: int=0, offset: bytes=hex(0)):
        self.name = name
        self.size = size
        self.offset = offset
        
        self.short_name, self.description = self.marker_info.get(
            self.name, ('', ''))
        
    def __str__(self):
        return 'Marker: {0}({1})\r\n{2}\r\nsize: {3}\r\noffset: {4}'.format(
                    self.name,
                    self.short_name,
                    self.description,
                    self.size,
                    self.offset
               )
    

class JpegParser:
    """
    Парсит jpeg-и и выводит информацию о них через
    соответствующие методы.
    """
    MARKER_NAME_FORMAT = '=2s'
    MARKER_SIZE_FORMAT = '>H'
    Marker = collections.namedtuple(
        'MarkerHeader', ('name', 'size'))
    READING_SIZE = 2    

    BASELINE_FORMAT = ">HBHHB"
    BASELINE_COMPONENT_FORMAT = ">BBB"
    BaselineDCT = collections.namedtuple(
        'BaselineDCT', 
        ('size', 'precision', 'height', 'width', 'amount_args'))
    BaselineComponent = collections.namedtuple(
        'BaselineComponent', 
        ('id', 'h_resampling', 'v_resampling', 'q_table_id')
    )
    
    def __init__(self, filename: str):
        self.jpeg_filename = filename
        self.markers = dict((
            (marker_info.name, marker_info) 
            for marker_info in self.define_markers()))
        
    def get_sof0_info(self):
        sos_info = self.markers.get(MarkerInfo.SOF0, MarkerInfo(b'\xff\xc0'))
        with open(self.jpeg_filename, 'rb') as f:
            f.seek(int(sos_info.offset, 16) + self.READING_SIZE)
            data = struct.unpack(
                self.BASELINE_FORMAT, f.read(8))
            baseline = self.BaselineDCT(*data)
            components = []
            for _ in range(baseline.amount_args):
                comp_id, resampling, q_table_id = struct.unpack(
                    self.BASELINE_COMPONENT_FORMAT,
                    f.read(3))
                components.append(self.BaselineComponent(
                    comp_id, resampling // 16, resampling % 16, q_table_id))
            return {
                'BaselineDCT': baseline, 
                'Components': components
            }
    
    def get_image_size(self):
        baseline = self.get_sof0_info()['BaselineDCT']
        return baseline.width, baseline.height
        
    def define_markers(self):
        """
        Возвращает информацию о каждом маркере в изображении.
        return: iter(MarkerInfo)
        """
        # firstly checking that file is jpeg, then truncate first 2 bytes
        soi = self.get_soi(self.jpeg_filename)
        if not soi:
            return
        with open(self.jpeg_filename, 'rb') as f:
            for buf in iter(lambda: f.read(self.READING_SIZE), b''):
                name = struct.unpack(self.MARKER_NAME_FORMAT, buf)[0]
                if name == MarkerInfo.SOI or name == MarkerInfo.EOI: 
                    yield MarkerInfo(name)
                    continue 
                cur_offset = hex(f.tell() - self.READING_SIZE)
                size = struct.unpack(
                    self.MARKER_SIZE_FORMAT, 
                    f.read(self.READING_SIZE))[0]               
                yield MarkerInfo(name=name, size=size, offset=cur_offset) 
                if name == MarkerInfo.SOS:
                    break
                next_offset = f.tell() + size - self.READING_SIZE
                f.seek(next_offset, 0)
            image_data = f.read()
            eoi = image_data.rfind(MarkerInfo.EOI)
            eoi_offset = hex(f.tell() - (len(image_data) - eoi))
            yield MarkerInfo(MarkerInfo.EOI, offset=eoi_offset)
    
    @staticmethod            
    def get_soi(filename: str):
        with open(filename, 'br') as f:
            data = f.read(2) 
            if data != b'\xff\xd8':
                return b''
            return data
            
                    
def main():
    parser = argparse.ArgumentParser(
        usage='{} [OPTIONS] FILE'.format(os.path.basename(sys.argv[0])),
        description='Jpeg descriptor')
    parser.add_argument('-m', '--markers', action='store_true', dest='markers',
                        help='list info about markers in file')
    parser.add_argument('-s', '--sof0_info', action='store_true', dest='sof0',
                        help='list info about start of frame (baselineDCT)')
    parser.add_argument('--size', action='store_true', dest='size',
                        help='show image size')
    parser.add_argument('fn', metavar='FILE',
                        help='name of an archive')

    args = parser.parse_args()
    if not (args.markers or 
            args.sof0 or
            args.size):
        sys.exit("Error: action must be specified")
    try:
        jpeg = JpegParser(args.fn)
        if args.markers:
            for info in jpeg.define_markers():
                print(info, end='\r\n\r\n')
        elif args.sof0:
            print(jpeg.get_sof0_info())
        elif args.size:
            print('x'.join(map(str, jpeg.get_image_size())))
    except Exception as e:
        sys.exit(e)

if __name__ == '__main__':
    main()